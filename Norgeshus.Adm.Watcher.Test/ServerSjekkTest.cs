﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Management;
using System.Security.Policy;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Norgeshus.Adm.Watcher.Utils.Disk;
using Norgeshus.Adm.Watcher.Utils.Shared;
using Quartz;
using Quartz.Impl;

namespace Norgeshus.Adm.Watcher.Test
{
    [TestClass]
    public class ServerSjekkTest
    {
        private List<ServerConfigMapConfigElement> hentServereFraConfig()
        {
            var servers = ServerConfigMapSection.Config.SettingsList.ToList();
            return servers;
        }

        [TestMethod]
        public void SjekkDisker()
        {
            DiskSpaceUtils utils = new DiskSpaceUtils();
            utils.SjekkServerDisker();
        }

        [TestMethod]
        public void Sjekk()
        {
            double Free, Size, FreePercentage;
            DateTime Now = DateTime.Now;

            string scopeStr = string.Format(@"root\cimv2", "10.200.32.9");


            ManagementScope scope = new ManagementScope(scopeStr);
            scope.Connect();

            string queryString = "SELECT * FROM Win32_Volume WHERE DriveLetter='e:'";
            SelectQuery query = new SelectQuery(queryString);
            using (ManagementObjectSearcher searcher = new ManagementObjectSearcher(scope, query))
            {
                Debug.WriteLine("Entering Volume loop: ");
                foreach (ManagementObject disk in searcher.Get())
                {
                    Debug.WriteLine("foreach Volume: ");
                    //-------------------------------------------------------------------------
                    //Debug.WriteLine("Free %" + double.Parse(disk["FreeSpace"].ToString()) / double.Parse(disk["Capacity"].ToString()) * 100);
                    FreePercentage = double.Parse(disk["FreeSpace"].ToString()) / double.Parse(disk["Capacity"].ToString()) * 100;
                    //-------------------------------------------------------------------------
                    string _MountPoint = disk["Name"].ToString();
                    //Debug.WriteLine("Free: " + disk["FreeSpace"].ToString());
                    Free = Math.Round(Convert.ToDouble(disk["FreeSpace"]) / (1024 * 1024), 2);
                    Debug.WriteLine("Free: " + Free + " MB");
                    //Debug.WriteLine("Capacity: " + disk["Capacity"].ToString());
                    Size = Math.Round(Convert.ToDouble(disk["Capacity"]) / (1024 * 1024), 2);
                    Debug.WriteLine("Size: " + Size + " MB");
                    if (_MountPoint[_MountPoint.Length - 1] == Path.DirectorySeparatorChar)
                    {
                        _MountPoint = _MountPoint.Remove(_MountPoint.Length - 1);
                    }
                    _MountPoint = _MountPoint.Replace("\\", "\\\\\\\\");

                    string _MountPointQueryString = "select * FROM Win32_MountPoint WHERE Directory=\"Win32_Directory.Name=\\\"" + _MountPoint + "\\\"\"";

                    SelectQuery _MountPointQuery = new SelectQuery(_MountPointQueryString);
                    using (
                        ManagementObjectSearcher mpsearcher =
                            new ManagementObjectSearcher(scope, _MountPointQuery))
                    {
                        Debug.WriteLine("Entering directory Foreach loop: ");
                        foreach (ManagementObject mp in mpsearcher.Get())
                        {
                            Debug.WriteLine("Foreach directory: ");

                            try
                            {
                                //Debug.WriteLine("Volume: " + mp["Volume"].ToString());
                                Debug.WriteLine("Directory: " + mp["Directory"].ToString());
                                string Volume = mp["Directory"].ToString().Replace("Win32_Directory.Name=", "");


                                if (FreePercentage <= 5.00)
                                {
                                    throw new Exception("\nLabel: " + Volume + "\nSeverity: " + EventLogEntryType.Error + "\nTime: " + Now + "\nMessage: disk space threshhold: " + CalculateUsedSpace(Free, Size) + " % used (" + Free + "MB" + " free)");
                                }

                            }
                            catch (Exception ex)
                            {
                                EventLog.WriteEntry("DriveStats Warning", "Message: " + ex.Message, EventLogEntryType.Error);
                            }
                        }
                    }
                }
            }
        }

        static double CalculateUsedSpace(double f, double s)
        {
            double UsedPercentage;
            if (s == 0)
            {
                return f;
            }
            else if (s >= 0 && f == 0)
            {
                UsedPercentage = 100.00;
                return UsedPercentage;
            }
            else
            {
                double UsedSpace = s - f;
                UsedPercentage = (UsedSpace / s) * 100;
                UsedPercentage = Math.Round(UsedPercentage, 2);
                //Debug.WriteLine("Used Percentage: " + UsedPercentage);
                return Math.Round(UsedPercentage, 2);
            }
        }

        static double CalculateFreePercentage(double f, double s)
        {
            double FreePercentage;
            if (f == 0)
            {
                FreePercentage = 0;
                return FreePercentage;
            }
            else
            {
                FreePercentage = (f / s) * 100;
                //Debug.WriteLine("Free Percentage: " + FreePercentage);
                return Math.Round(FreePercentage, 2);
            }

        }

        [TestMethod]
        public void LogicalDisks()
        {
            ManagementPath path = new ManagementPath()
            {
                NamespacePath = @"root\cimv2",
                Server = "10.200.32.38"
            };
            ManagementScope scope = new ManagementScope(path);
            SelectQuery serverQuery = new SelectQuery("SELECT * FROM Win32_DiskDrive");
            using (ManagementObjectSearcher search = new ManagementObjectSearcher(scope, serverQuery))
            {
                // extract model and interface information
                foreach (ManagementObject drive in search.Get())
                {
                    string antecedent = drive["DeviceID"].ToString(); // the disk we're trying to find out about
                    antecedent = antecedent.Replace(@"\", "\\"); // this is just to escape the slashes
                    string query = "ASSOCIATORS OF {Win32_DiskDrive.DeviceID='" + antecedent + "'} WHERE AssocClass = Win32_DiskDriveToDiskPartition";
                    using (ManagementObjectSearcher partitionSearch = new ManagementObjectSearcher(query))
                    {
                        foreach (ManagementObject part in partitionSearch.Get())
                        {
                            var a = part.GetPropertyValue("a");
                        }
                    }
                }
            }
        }
    }
}
