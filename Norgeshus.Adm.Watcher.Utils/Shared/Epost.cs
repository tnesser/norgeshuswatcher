﻿using System;
using System.Linq;
using NLog;
using Norgeshus.Adm.Watcher.Utils.MSExchangeService;

namespace Norgeshus.Adm.Watcher.Utils.Shared
{
    public class Epost
    {
        private static Logger _loggerNph = LogManager.GetLogger("NPH");
        public void SendEpost(string tittel, string melding, EpostMottakerListe mottakere, string brukernavn)
        {
            try
            {
                MSExchangeContractClient epost = new MSExchangeContractClient();

                SendEpostUtenLagringRequest request = new SendEpostUtenLagringRequest();
                request.Brukernavn = brukernavn;

                EpostEntitet epostEntitet = new EpostEntitet();
                epostEntitet.Tittel = tittel;
                epostEntitet.Til = mottakere;
                epostEntitet.Innhold = melding;
                epostEntitet.Fra = "support@norgeshus.no";

                request.Epost = epostEntitet;

                epost.SendEpostUtenLagring(request);
            }
            catch (Exception ex)
            {
                _loggerNph.Error(ex, "Sending av epost feilet: ");
                _loggerNph.Info($"Tittel: '{tittel}'");
                if (mottakere.Any())
                {
                    _loggerNph.Info("Mottakere:");
                    foreach (EpostMottaker em in mottakere)
                    {
                        _loggerNph.Info($"-'{em.EpostAdresse}'");
                    }
                }
                else
                {
                    _loggerNph.Info("Mottakere: Ingen");
                }
                _loggerNph.Info($"Melding: '{melding}'");

                throw new Exception("Sending av epost feilet", ex);
            }
        }
    }
}
