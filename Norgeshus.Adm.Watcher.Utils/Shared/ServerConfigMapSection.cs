﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace Norgeshus.Adm.Watcher.Utils.Shared
{
    public sealed class ServerConfigMapSection : ConfigurationSection
    {
        private static ServerConfigMapSection config = ConfigurationManager.GetSection("Servers") as ServerConfigMapSection;

        public static ServerConfigMapSection Config
        {
            get
            {
                return config;
            }
        }

        [ConfigurationProperty("", IsRequired = true, IsDefaultCollection = true)]
        private ServerConfigMapConfigElements Settings
        {
            get { return (ServerConfigMapConfigElements)this[""]; }
            set { this[""] = value; }
        }

        public IEnumerable<ServerConfigMapConfigElement> SettingsList
        {
            get { return this.Settings.Cast<ServerConfigMapConfigElement>(); }
        }
    }

    public sealed class ServerConfigMapConfigElements : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new ServerConfigMapConfigElement();
        }
        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((ServerConfigMapConfigElement)element).Key;
        }
    }

    public sealed class ServerConfigMapConfigElement : ConfigurationElement
    {
        [ConfigurationProperty("key", IsKey = true, IsRequired = true)]
        public string Key
        {
            get { return (string)base["key"]; }
            set { base["key"] = value; }
        }

        [ConfigurationProperty("ip", IsKey = true, IsRequired = true)]
        public string IP
        {
            get { return (string)base["ip"]; }
            set { base["ip"] = value; }
        }

        [ConfigurationProperty("navn", IsKey = true, IsRequired = true)]
        public string Navn
        {
            get { return (string)base["navn"]; }
            set { base["navn"] = value; }
        }

        [ConfigurationProperty("disk", IsRequired = true)]
        public string Disk
        {
            get { return (string)base["disk"]; }
            set { base["disk"] = value; }
        }

        [ConfigurationProperty("grenseverdi_advarsel", IsRequired = true)]
        public string Grenseverdi_Advarsel
        {
            get { return (string)base["grenseverdi_advarsel"]; }
            set { base["grenseverdi_advarsel"] = value; }
        }

        [ConfigurationProperty("grenseverdi_kritisk", IsRequired = true)]
        public string Grenseverdi_Kritisk
        {
            get { return (string)base["grenseverdi_kritisk"]; }
            set { base["grenseverdi_kritisk"] = value; }
        }
    }
}

