﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading.Tasks;
using NLog;
using Norgeshus.Adm.Watcher.Utils.MSExchangeService;
using Norgeshus.Adm.Watcher.Utils.Shared;

namespace Norgeshus.Adm.Watcher.Utils.Disk
{
    public class DiskSpaceUtils
    {
        protected static Logger logger_advarsel = LogManager.GetLogger("WARN");
        protected static Logger logger_kritisk = LogManager.GetLogger("ERROR");
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public async Task<bool> SjekkServerDisker()
        {
            logger.Debug("Henter opp servere");
            var servere = ServerConfigMapSection.Config.SettingsList.ToList();
            logger.Debug($"Fant {servere.Count} servere");
            if (servere.Any())
            {
                foreach (ServerConfigMapConfigElement sce in servere)
                {
                    try
                    {
                        logger.Debug($"Sjekker server {sce.Navn}");
                        var diskinfo = getDiskSpace(sce.IP, sce.Disk);
                        long grenseverdi_advarsel = long.Parse(sce.Grenseverdi_Advarsel);
                        long grenseverdi_kritisk = long.Parse(sce.Grenseverdi_Kritisk);

                        if (diskinfo.freeSpace == -1)
                        {
                            logger_advarsel.Warn($"Fikk ikke sjekket {sce.Navn} ({sce.Disk}:)");
                            logger.Warn($"Fikk ikke sjekket {sce.Navn} ({sce.Disk}:)");
                        }

                        long freeSpace = diskinfo.freeSpace;
                        string strFreeSpace = freeSpace.ToPrettySize(1);

                        logger.Debug($"{sce.Navn}: {strFreeSpace}");

                        long capacity = diskinfo.capacity;
                        string strCapacity = capacity.ToPrettySize(1);

                        logger.Debug($"{sce.Navn}: {strCapacity}");
                        // prosent ledig
                        double prosent = ((double)freeSpace / (double)capacity) * 100;
                        if (freeSpace < grenseverdi_kritisk)
                        {
                            // Kritisk nivå
                            logger_kritisk.Error(
                                $"Det er veldig lite disk tilgjenglig på {sce.Navn} ({sce.IP}): {strFreeSpace} ({prosent:N1}%)");
                            logger.Error(
                                $"Det er veldig lite disk tilgjenglig på {sce.Navn} ({sce.IP}): {strFreeSpace} ({prosent:N1}%)");

                            // Send epost
                            string[] mottakere = ConfigurationManager.AppSettings["mottakere_kritisk"].Split(',');
                            if (mottakere.Length > 0)
                            {
                                // Mottakere
                                List<string> mottakerliste = mottakere.ToList();
                                EpostMottakerListe epostmottakere = new EpostMottakerListe();
                                foreach (string mottaker in mottakerliste)
                                {
                                    epostmottakere.Add(new EpostMottaker() { EpostAdresse = mottaker.Trim() });
                                }

                                // Melding
                                StringBuilder innhold = new StringBuilder();
                                string timestamp = DateTime.Now.ToString("dd.mm.yyyy HH:mm:ss");
                                innhold.AppendFormat($"<h3>{timestamp} - Ledig disk {sce.Navn} ({sce.IP}) {sce.Disk}:</h3>");
                                innhold.AppendFormat($"Ledig plass:{strFreeSpace} av {strCapacity} ({prosent:N1}% ledig)</br>");

                                Epost epost = new Epost();
                                epost.SendEpost("DISKPLASS - KRITISK", innhold.ToString(), epostmottakere, "kpduser");
                            }
                        }
                        else if (freeSpace < grenseverdi_advarsel)
                        {
                            // Advarsel nivå
                            logger_advarsel.Warn($"Det er lite disk tilgjenglig på  {sce.Navn} ({sce.IP}) {sce.Disk}: {strFreeSpace} ({prosent:N1}%)");
                            logger.Warn($"Det er lite disk tilgjenglig på  {sce.Navn} ({sce.IP}) {sce.Disk}: {strFreeSpace} ({prosent:N1}%)");
                        }
                    }
                    catch (Exception e)
                    {
                        logger.Error(e, $"Feilet: {e.Message}", sce.Navn);

                        return false;
                    }
                }

                return true;
            }

            return false;
        }


        private (long freeSpace, long capacity) getDiskSpace(string server, string driveLetter)
        {
            try
            {
                long longFreeSpace = -1;
                long longCapicity = -1;

                ManagementPath path = new ManagementPath()
                {
                    NamespacePath = @"root\cimv2",
                    Server = server
                };
                ManagementScope scope = new ManagementScope(path);
                string condition = $"DriveLetter = '{driveLetter}:'";
                string[] selectedProperties = new string[] { "FreeSpace", "Capacity" };
                SelectQuery query = new SelectQuery("Win32_Volume", condition, selectedProperties);

                using (ManagementObjectSearcher searcher = new ManagementObjectSearcher(scope, query))
                using (ManagementObjectCollection results = searcher.Get())
                {
                    ManagementObject volume = results.Cast<ManagementObject>().SingleOrDefault();

                    if (volume != null)
                    {
                        ulong freeSpace = (ulong)volume.GetPropertyValue("FreeSpace");
                        longFreeSpace = (long)freeSpace;

                        ulong capacity = (ulong)volume.GetPropertyValue("Capacity");
                        longCapicity = (long)capacity;

                        return (freeSpace: longFreeSpace, capacity: longCapicity);
                    }
                }

                return (freeSpace: longFreeSpace, capacity: longCapicity);
            }
            catch (Exception e)
            {
                return (freeSpace: -1, capacity: -1);
            }
        }

        public void LogicalMounts()
        {
            ManagementPath path = new ManagementPath()
            {
                NamespacePath = @"root\cimv2",
                Server = "10.200.32.38"
            };
            ManagementScope scope = new ManagementScope(path);
            SelectQuery serverQuery = new SelectQuery("SELECT * FROM Win32_DiskDrive");
            using (ManagementObjectSearcher search = new ManagementObjectSearcher(scope, serverQuery))
            {
                // extract model and interface information
                foreach (ManagementObject drive in search.Get())
                {
                    string antecedent = drive["DeviceID"].ToString(); // the disk we're trying to find out about
                    antecedent = antecedent.Replace(@"\", "\\"); // this is just to escape the slashes
                    string query = "ASSOCIATORS OF {Win32_DiskDrive.DeviceID='" + antecedent + "'} WHERE AssocClass = Win32_DiskDriveToDiskPartition";
                    using (ManagementObjectSearcher partitionSearch = new ManagementObjectSearcher(query))
                    {
                        foreach (ManagementObject part in partitionSearch.Get())
                        {
                            logger.Warn($"Name: {part["Name"]}");
                            logger.Warn($"Name: {part["FreeSpace"]}");
                            logger.Warn($"Name: {part["Capacity"]}");
                        }
                    }
                }

            }
        }
    }
}
