﻿using System;
using Topshelf;

namespace Norgeshus.Adm.Watcher.Service
{
    static class Program
    {
        public static void Main()
        {
            var rc = HostFactory.Run(x =>
            {
                x.EnableShutdown();
                x.StartAutomatically();
                x.UseNLog();
                x.Service<WatcherService>(s =>
                {
                    s.ConstructUsing(name => new WatcherService());
                    s.WhenStarted(tc => tc.Start());
                    s.WhenStopped(tc => tc.Stop());
                });
                x.RunAsLocalSystem();

                x.SetDescription("Norgeshus Watcherservice");
                x.SetDisplayName("Norgeshus Watcherservice");
                x.SetServiceName("NorgeshusWatcherService");
            });

            var exitCode = (int)Convert.ChangeType(rc, rc.GetTypeCode());  
            Environment.ExitCode = exitCode;
        }
    }
}
