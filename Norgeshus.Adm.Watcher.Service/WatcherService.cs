﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using NLog;
using Norgeshus.Adm.Watcher.Service.Jobs;
using Quartz;
using Quartz.Impl;
using Quartz.Impl.Matchers;
using Quartz.Impl.Triggers;
using Quartz.Logging;
using Logger = NLog.Logger;

namespace Norgeshus.Adm.Watcher.Service
{
    public class WatcherService
    {
        static Logger logger = LogManager.GetLogger("SERVICE");
        private RedisSubscriberJob job;
        private Timer m_mainTimer;
        private ISchedulerFactory sf;
        private IScheduler sched;

        public void Start()
        {
            sf = new StdSchedulerFactory();
            sched = sf.GetScheduler().Result;

            logger.Info("Starter Watcher service");

            LogProvider.SetCurrentLogProvider(new NLogProvider());

            registrerJobber().Wait();

           // GetAllJobs(sched);
        }

        public void Stop()
        {
            logger.Info("Stopper Watcher service");
            sf = new StdSchedulerFactory();
            sched = sf.GetScheduler().Result;
            sched.Shutdown(waitForJobsToComplete: true);
        }

        private async Task registrerJobber()
        {
            logger.Info("------- Starter Scheduler -----------------");
            logger.Info("Initialiserer Quartz.net");

            // First we must get a reference to a scheduler
            
            await sched.Start();

            logger.Info("Initialisering Ferdig");

            logger.Info("Registrerer skedulerte jobber");

            // define the job and tie it to our HelloJob class
            IJobDetail job = JobBuilder.Create<ServerSjekkJob>()
                .WithIdentity("sjekkServereJobb", "sjekkServereGruppe")
                .Build();

            // Trigger the job to run now, and then every 40 seconds
            ITrigger trigger = TriggerBuilder.Create()
                .WithIdentity("sjekkServereTrigger", "sjekkServereGruppe")
                .StartNow()
                .WithSimpleSchedule(x => x
                    .WithIntervalInHours(1)
                    .RepeatForever())
                .Build();

            await sched.ScheduleJob(job, trigger);

            logger.Info("------- Startet Scheduler -----------------");
        }

        private async void nySkedulertJob<T>(string jobbnavn, string schedulerGruppenavn, string triggernavn, string appSettingsKey, bool kjor) where T : IJob
        {
            IJobDetail nyJob = JobBuilder.Create<T>()
                .WithIdentity(jobbnavn, schedulerGruppenavn)
                .Build();

            // Kjører Nytthus jobben hvert xx. sekund
            // string schedule = System.Configuration.ConfigurationManager.AppSettings[appSettingsKey];

            //if (kjor)
            //{
            //    ITrigger trigger = TriggerBuilder.Create()
            //        .WithIdentity(triggernavn, schedulerGruppenavn)
            //        .StartNow()
            //        .WithSimpleSchedule(x => x
            //            .WithIntervalInHours(3)
            //            .RepeatForever())
            //        .Build();

            //    try
            //    {
            //        logger.Info("Skedulerer jobb");
            //        await sched.ScheduleJob(nyJob, trigger);
            //    }
            //    catch (Exception e)
            //    {
            //        logger.Error(e, $"Skedulering av jobb feilet {e.Message}");
            //    }
            //}
            //else
            //{
            //    ITrigger trigger = TriggerBuilder.Create()
            //        .WithIdentity(triggernavn, schedulerGruppenavn)
            //        .WithSimpleSchedule(x => x
            //            .WithIntervalInMinutes(1)
            //            .RepeatForever())
            //        .Build();

            //    try
            //    {
            //        logger.Info("Skedulerer jobb");
            //        await sched.ScheduleJob(nyJob, trigger);
            //        logger.Info($"{nyJob.Key} vil kjøre: {trigger.GetNextFireTimeUtc()}");
            //    }
            //    catch (Exception e)
            //    {
            //        logger.Error(e, $"Skedulering av jobb feilet {e.Message}");
            //    }
            //}
        }

        private void startRedisSubscriberJob()
        {
            job = new RedisSubscriberJob();
            job.Start();
        }

        private static void GetAllJobs(IScheduler scheduler)
        {
            var jobGroups = scheduler.GetJobGroupNames().Result;
            // IList<string> triggerGroups = scheduler.GetTriggerGroupNames();

            logger.Info("Kjørende jobber:");
            foreach (string group in jobGroups)
            {
                var groupMatcher = GroupMatcher<JobKey>.GroupContains(group);
                var jobKeys = scheduler.GetJobKeys(groupMatcher).Result;
                foreach (var jobKey in jobKeys)
                {
                    var detail = scheduler.GetJobDetail(jobKey);
                    var triggers = scheduler.GetTriggersOfJob(jobKey).Result;
                    foreach (ITrigger trigger in triggers)
                    {
                        logger.Info(group);
                        logger.Info(jobKey.Name);
                        logger.Info(trigger.Key.Name);
                        logger.Info(trigger.Key.Group);
                        logger.Info(trigger.GetType().Name);
                        logger.Info(scheduler.GetTriggerState(trigger.Key));
                        DateTimeOffset? nextFireTime = trigger.GetNextFireTimeUtc();
                        if (nextFireTime.HasValue)
                        {
                            logger.Info(nextFireTime.Value.LocalDateTime.ToString());
                        }

                        DateTimeOffset? previousFireTime = trigger.GetPreviousFireTimeUtc();
                        if (previousFireTime.HasValue)
                        {
                            logger.Info(previousFireTime.Value.LocalDateTime.ToString());
                        }
                    }
                }
            }

        public class NLogProvider : ILogProvider
        {
            public IDisposable OpenNestedContext(string message)
            {
                throw new NotImplementedException();
            }

            public IDisposable OpenMappedContext(string key, string value)
            {
                throw new NotImplementedException();
            }

            Quartz.Logging.Logger ILogProvider.GetLogger(string name)
            {
                return (level, func, exception, parameters) =>
                {
                    if (level >= Quartz.Logging.LogLevel.Info && func != null)
                    {
                        logger.Info("[" + DateTime.Now.ToLongTimeString() + "] [" + level + "] " + func(), parameters);
                    }

                    return true;
                };
            }
        }
    }
}
