﻿namespace Norgeshus.Adm.Watcher.Service.Jobs
{
    internal class RedisSubscriberJob
    {
        private const int REDISMQ = 4;

        public void Start()
        {
            //epost = new Epost();
         //   epost = new Epost();

         //   signalRClient.RunAsync().Wait();

         //   // Best practice https://gist.github.com/JonCole/925630df72be1351b21440625ff2671f

         //   RedisConnection.ConnectionFailed += (sender, e) =>
         //   {
         //       logger.Info("Oppkobling til Redis feilet: " + _redisServer);
         //       if (e.Exception != null)
         //           logger.Info(e.Exception, $"Feilmelding: {e.Exception.Message}");
         //   };

         //   db = RedisConnection.GetDatabase(REDISMQ);

         //   /*  Persistering, bruke pubsub til å publisere "nymelding", men hente input fra en annen redisdb
         //*  https://github.com/StackExchange/StackExchange.Redis/blob/master/Docs/PipelinesMultiplexers.md
         //*  sub.Subscribe(channel, delegate {
         //   string work = db.ListRightPop(key);
         //   if (work != null) Process(work);
         //   });
         //   //...
         //   db.ListLeftPush(key, newWork, flags: CommandFlags.FireAndForget);
         //   sub.Publish(channel, "");
         //   */

         //   // Lytter på NPHFiloverforing Channel
         //   RedisConnection.GetSubscriber().Subscribe(PubSub.NPH_Filoverforing, (channel, value) => FiloverforingsMeldingMottatt(value));

         //   // Lytter på NPHFilnedlasting Channel
         //   RedisConnection.GetSubscriber().Subscribe(PubSub.NPH_Filnedlasting, (channel, value) => FilnedlastningMeldingMottatt(value));

         //   // Lytter på prosjekthotell send epost Channel
         //   RedisConnection.GetSubscriber().Subscribe(PubSub.NPH_Epost, (channel, value) => NPHSendEpost(value));
        }
    }
}
