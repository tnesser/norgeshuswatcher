﻿using System.Threading.Tasks;
using NLog;
using Norgeshus.Adm.Watcher.Utils.Disk;
using Quartz;

namespace Norgeshus.Adm.Watcher.Service.Jobs
{
    public class ServerSjekkJob : IJob
    {
        public static Logger logger = LogManager.GetLogger("SERVICE");

        public async Task Execute(IJobExecutionContext context)
        {
            logger.Debug("Starter - serversjekk");
            DiskSpaceUtils utils = new DiskSpaceUtils();
            var suksess = await utils.SjekkServerDisker();
            if(suksess)
                logger.Debug("serversjekk : suksess");
            else
                logger.Debug("serversjekk : ikke suksess");

            logger.Debug("Ferdig - serversjekk");
        }
    }
}